﻿Create Table ResearchGroup(
    id int PRIMARY KEY,
    name varchar(100) NOT NULL,
	url varchar(200),
	phone varchar(30),
	[DeparmentId] int NULL, --FOREIGN KEY
	[ResearchCenterId] int NULL, --FOREIGN KEY
	FOREIGN KEY ([DeparmentId]) REFERENCES Deparment(id) ON DELETE SET NULL,
	FOREIGN KEY ([ResearchCenterId]) REFERENCES ResearchCenter(id) ON DELETE SET NULL
);