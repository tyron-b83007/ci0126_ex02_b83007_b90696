﻿Create Table University (
    id int PRIMARY KEY IDENTITY,
    name varchar(100) NOT NULL,
	url varchar(200),
	[CountryId] int NULL, --FOREIGN KEY
	FOREIGN KEY ([CountryId]) REFERENCES Country(id) ON DELETE SET NULL
);