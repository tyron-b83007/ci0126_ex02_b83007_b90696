﻿Create Table Deparment(
	id int PRIMARY KEY,
	name varchar(100) NOT NULL,
	[UniversityId] int NULL, --FOREIGN KEY
	FOREIGN KEY ([UniversityId]) REFERENCES University(id) ON DELETE SET NULL
);