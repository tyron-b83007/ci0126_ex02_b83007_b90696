﻿CREATE TABLE [dbo].[CollaboratorPerson]
(
	Name nvarchar(60) NOT NULL PRIMARY KEY, 
    [Gender] CHAR(1) NOT NULL, 
    [Role] NCHAR(30) NOT NULL, 
    [ResearchGroupId] INT NOT NULL,
    CONSTRAINT [FK_Person_Group] FOREIGN KEY ([ResearchGroupId]) REFERENCES [ResearchGroup]([Id])
)
