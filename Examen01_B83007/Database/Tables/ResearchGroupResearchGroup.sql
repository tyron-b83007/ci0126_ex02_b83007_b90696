﻿Create Table ResearchGroupResearchGroup(
    [Id] int NOT NULL, --FOREIGN KEY
	[ResearchGroupId1] int NOT NULL, --FOREIGN KEY
	FOREIGN KEY ([Id]) REFERENCES ResearchGroup(id),
	FOREIGN KEY ([ResearchGroupId1]) REFERENCES ResearchGroup(id),
    PRIMARY KEY ([Id], [ResearchGroupId1])
);