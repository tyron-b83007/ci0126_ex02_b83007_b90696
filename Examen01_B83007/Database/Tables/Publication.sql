﻿CREATE TABLE [dbo].[Publication]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Title] NVARCHAR(150) NOT NULL, 
    [DOI] NVARCHAR(300) NULL,
    [ResearchGroupId] INT NOT NULL,
    CONSTRAINT [FK_Publication_Group] FOREIGN KEY ([ResearchGroupId]) REFERENCES [ResearchGroup]([Id])
)
