﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace Selenium.Universities.Pages
{
    [TestClass]
    public class UniversitiesListTests
    {
        IWebDriver driver;

        [TestCleanup]
        public void TearDown()
        {
            if (driver != null)
                driver.Quit();
        }

        [TestMethod]
        public void ListarUniversidadesChrome()
        {
            driver = new ChromeDriver();
            ListarUniversidades();
        }


        [TestMethod]
        public void ListarUniversidadesFirefox()
        {
            var op = new FirefoxOptions
            {
                AcceptInsecureCertificates = true
            };
            driver = new FirefoxDriver(op);
            ListarUniversidades();
        }

        public void ListarUniversidades()
        {
            ///arrange 
            driver.Manage().Window.Maximize();

            //act
            driver.Navigate().GoToUrl("https://localhost:44373/universities");

            //assert
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-body > .mud-table-row:nth-child(1) > .mud-table-cell:nth-child(1)")).Text, "Universidade Federal do Rio de Janeiro");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-row:nth-child(2) > .mud-table-cell:nth-child(1)")).Text, "Universidad Politécnica de Valencia");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-row:nth-child(3) > .mud-table-cell:nth-child(1)")).Text, "North Carolina State University");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-row:nth-child(4) > .mud-table-cell:nth-child(1)")).Text, "Universidad Estatal a Distancia de Costa Rica");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-row:nth-child(5) > .mud-table-cell:nth-child(1)")).Text, "Universidad Nacional de Costa Rica");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-row:nth-child(6) > .mud-table-cell:nth-child(1)")).Text, "Instituto Tecnológico de Costa Rica");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-row:nth-child(7) > .mud-table-cell:nth-child(1)")).Text, "Universidad de Costa Rica");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-table-page-number-information")).Text, "1-7 of 7");
            driver.FindElement(By.CssSelector(".mud-table-toolbar")).Click();
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-typography-h6")).Text, "Lista de Universidades registradas");

        }

        [TestMethod]
        public void DespliegaMsjDeConfirmacionChrome()
        {
            driver = new ChromeDriver();
            DespliegaMsjDeConfirmacion();
        }


        [TestMethod]
        public void DespliegaMsjDeConfirmacionFirefox()
        {
            var op = new FirefoxOptions
            {
                AcceptInsecureCertificates = true
            };
            driver = new FirefoxDriver(op);
            DespliegaMsjDeConfirmacion();
        }

        public void DespliegaMsjDeConfirmacion()
        {
            ///arrange 
            driver.Manage().Window.Maximize();

            //act
            driver.Navigate().GoToUrl("https://localhost:44373/universities");

            //assert
            driver.FindElement(By.CssSelector(".mud-table-row:nth-child(1) .mud-tooltip-root:nth-child(2) .mud-icon-root")).Click();
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-typography-h4")).Text, "¿Desea eliminar la universidad: Universidade Federal do Rio de Janeiro?");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-button-filled-primary > .mud-button-label")).Text, "ELIMINAR UNIVERSIDAD");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-button-filled-error > .mud-button-label")).Text, "CERRAR");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".mud-alert-message")).Text, "Si elimina la universidad todos los grupos asociados se eliminarán");
            driver.FindElement(By.CssSelector(".mud-button-filled-error > .mud-button-label")).Click();

        }
    }
}
