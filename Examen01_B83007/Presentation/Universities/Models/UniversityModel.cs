﻿using Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Universities.Models
{
    public class UniversityModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Url { get; set; }

        public Country? Country { get; set; }

        public UniversityModel(int id, string name, string? url)
        {
            Id = id;
            Name = name;
            Url = url;
        }

        public UniversityModel()
        {
            Id = 0;
            Name = null!;
            Url = null!;
        }
    }
}
