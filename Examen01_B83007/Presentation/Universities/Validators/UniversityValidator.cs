﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using FluentValidation;
using Presentation.Universities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Universities.Validators
{
    public class UniversityValidator : AbstractValidator<UniversityModel>
    {
        private int nameMaxLength = 100;
        private int urlnMaxLength = 200;
        public UniversityValidator()
        {
            RuleFor(p => p.Name)
            .Custom((name, context) =>
            {
                var result = RequiredString.TryCreate(name);
                if (!result.IsFail)
                    return;

                var error = result.Fail();
                var errorMessage = error switch
                {
                    RequiredString.IsNullOrWhitespace => "Por favor ingrese un nombre para la universidad",
                    RequiredString.TooLong tooLong => $"Ingrese menos de {nameMaxLength} caracteres",
                    _ => throw new ArgumentOutOfRangeException(nameof(error))
                };

                context.AddFailure(nameof(UniversityModel.Name), errorMessage);
            });

            RuleFor(p => p.Url).NotEmpty().WithMessage("Por favor ingrese un url para la página web de la universidad");
            RuleFor(p => p.Url).MaximumLength(urlnMaxLength).WithMessage($"Ingrese menos de {nameMaxLength} caracteres");
        }

        public Func<object, string, Task<IEnumerable<string>>> ValidateValue => async (model, propertyName) =>
        {
            var result = await ValidateAsync(ValidationContext<UniversityModel>.CreateWithOptions((UniversityModel)model, x => x.IncludeProperties(propertyName)));
            if (result.IsValid)
                return Array.Empty<string>();
            return result.Errors.Select(e => e.ErrorMessage);
        };
    }    
}
