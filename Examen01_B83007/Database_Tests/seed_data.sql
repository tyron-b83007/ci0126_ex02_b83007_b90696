﻿/*
Post-Deployment Script						
--------------------------------------------------------------------------------------
 Tyron Fonseca, B83007			
--------------------------------------------------------------------------------------
*/

DELETE FROM [dbo].[CollaboratorPerson];
DELETE FROM [dbo].[Thesis];
DELETE FROM [dbo].[Project];
DELETE FROM [dbo].[Publication];
DELETE FROM [dbo].[ResearchGroupResearchGroup];
DELETE FROM [dbo].[Deparment];
DELETE FROM [dbo].[University];
DELETE FROM [dbo].[Country];
DELETE FROM [dbo].[ResearchGroup];
DELETE FROM [dbo].[ResearchCenter];

/*=================  RESEARCH CENTER ================= */
MERGE INTO [dbo].[ResearchCenter] AS TARGET
USING(VALUES
    (1, 'CITIC', 'https://citic.ucr.ac.cr/')
)
AS SOURCE ([id], [name], [url]) ON TARGET.[id] = SOURCE.[id] AND TARGET.[name] = SOURCE.[name] AND TARGET.[url] = SOURCE.[url]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([id], [name], [url]) VALUES ([id], [name], [url]);

/*================= COUNTRY ================= */

MERGE INTO [dbo].[Country] AS TARGET
USING(VALUES
    (1, 'EEUU'),
    (2, 'España'),
    (3, 'Brasil'),
    (4, 'Costa Rica')
)
AS SOURCE ([id], [name]) ON TARGET.[id] = SOURCE.[id] AND TARGET.[name] = SOURCE.[name]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([id], [name]) VALUES ([id], [name]);

/*================= UNIVERSITY ================= */
SET IDENTITY_INSERT [dbo].[University] ON;
MERGE INTO [dbo].[University] AS TARGET
USING(VALUES
    (1, 'Universidad de Costa Rica', 'https://ucr.ac.cr/', 4),
    (2, 'Instituto Tecnológico de Costa Rica', 'https://tec.ac.cr/', 4),
    (3, 'Universidad Nacional de Costa Rica', 'https://una.ac.cr/', 4),
    (4, 'Universidad Estatal a Distancia de Costa Rica', 'https://uned.ac.cr/ecen', 4),
    (5, 'North Carolina State University', 'https://www.ncsu.edu/', 1),
    (6, 'Universidad Politécnica de Valencia', 'http://www.upv.es/en', 2),
    (7, 'Universidade Federal do Rio de Janeiro', 'https://ufrj.br/es/', 3)
)
AS SOURCE ([id], [name], [url], [CountryId]) ON TARGET.[id] = SOURCE.[id] AND TARGET.[name] = SOURCE.[name] AND TARGET.[url] = SOURCE.[url] AND TARGET.[CountryId] = SOURCE.[CountryId]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([id], [name], [url], [CountryId]) VALUES ([id], [name], [url], [CountryId]);

/*================= DEPARMENT ================= */

MERGE INTO [dbo].[Deparment] AS TARGET
USING(VALUES
    (1, 'Department of Computer Science', 5),
    (2, 'Escuela Técnica Superior de Ingeniería Informática', 6),
    (3, 'COPPE -  Programa de Engenharia de Sistemas e Computação', 7),
    (4, 'Unidad de Ingeniería en Computación', 2),
    (5, 'Escuela de Computación e informática', 3),
    (6, 'Escuela de Ciencias Exactas y Naturales', 4)
)
AS SOURCE ([id], [name], [UniversityId]) ON TARGET.[id] = SOURCE.[id] AND TARGET.[name] = SOURCE.[name] AND TARGET.[UniversityId] = SOURCE.[UniversityId]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([id], [name], [UniversityId]) VALUES ([id], [name], [UniversityId]);

/*================= RESEARCH GROUP ================= */
MERGE INTO [dbo].[ResearchGroup] AS TARGET
USING(VALUES
    (1, 'Grupo de investigación en ingeniería de software empírica', 'https://citic.ucr.ac.cr/esegroup', '+506 2511-0000',  null, 1),
    (2, 'Realsearch group', 'https://realsearchgroup.github.io/', null,  1, null),
    (3, 'Real-world Artificial Intelligence for Software Engineering', 'http://menzies.us/', null,  1, null),
    (4, 'Centro de Investigación en Métodos de Producción de Software (PROS)', 'http://www.pros.upv.es/', null,  2, null),
    (5, 'Experimental Software Engineering Group ', 'http://lens-ese.cos.ufrj.br/ese/', null,  3, null),
    (6, 'Protec', 'https://tec.ac.cr/', null,  4, null),
    (7, 'Programa de Bachillerato y Maestría', 'https://una.ac.cr/', null,  5, null),
    (8, 'Programa de Licenciaturas', 'https://www.uned.ac.cr/ecen', null,  6, null),
    (9, 'Interacción Humano Computador', 'https://citic.ucr.ac.cr/tags/interaccion-humano-computador', '+506 2511-0016',  null, 1),
    (10, 'Seguridad y Privacidad', 'https://citic.ucr.ac.cr/tags/seguridad', '+506 2511-0026',  null, 1)
)
AS SOURCE ([id], [name], [url], [phone], [DeparmentId], [ResearchCenterId]) ON TARGET.[id] = SOURCE.[id] AND TARGET.[name] = SOURCE.[name] AND TARGET.[url] = SOURCE.[url] AND TARGET.[DeparmentId] = SOURCE.[DeparmentId] AND TARGET.[ResearchCenterId] = SOURCE.[ResearchCenterId] AND TARGET.[phone] = SOURCE.[phone]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([id], [name], [url],[phone], [DeparmentId], [ResearchCenterId]) VALUES ([id], [name], [url],[phone], [DeparmentId], [ResearchCenterId]);

/*================= COLLABORATES ================= */

MERGE INTO [dbo].[ResearchGroupResearchGroup] AS TARGET
USING(VALUES
    (1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    (1, 6),
    (1, 7),
    (1, 8),
    (9, 3),
    (9, 4)
)
AS SOURCE ([Id], [ResearchGroupId1]) ON TARGET.[Id] = SOURCE.[Id] AND TARGET.[ResearchGroupId1] = SOURCE.[ResearchGroupId1]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Id], [ResearchGroupId1]) VALUES ([Id], [ResearchGroupId1]);


/*================= COLLABORATORS ================= */

MERGE INTO [dbo].[CollaboratorPerson] AS TARGET
USING(VALUES
    ('Dylan Arias', 'M', 'Estudiante', 1),
    ('Tyron Fonseca', 'M', 'Estudiante', 1),
    ('Sebastian Montero', 'M', 'Estudiante', 1),
    ('Greivin', 'M', 'Estudiante', 1),
    ('Andrea', 'F', 'Estudiante', 1),
    ('Jarod', 'M', 'Estudiante', 1),
    ('Cristian', 'M', 'Investigador', 1),
    ('Alexandra', 'F', 'Investigador', 1),
    ('Arturo', 'M', 'Investigador', 1),
    ('Edgar', 'M', 'Investigador', 1),
    ('Kryscia', 'F', 'Investigador', 1),
    ('Alan', 'M', 'Investigador', 1),
    ('Braulio', 'M', 'Investigador', 1),
    ('Carlos', 'M', 'Investigador', 1),
    ('Jeisson', 'M', 'Investigador', 1),
    ('Sandra', 'F', 'Investigador', 1),
    ('Roxana', 'F', 'Investigador', 1)
)
AS SOURCE ([Name], Gender, [Role], [ResearchGroupId]) ON TARGET.[Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Name], Gender, [Role], [ResearchGroupId]) VALUES ([Name], Gender, [Role], [ResearchGroupId]);

/*================= THSES ================= */

MERGE INTO [dbo].Thesis AS TARGET
USING(VALUES
    ('Titulo 1', 'Posgrado', 1),
    ('Titulo 2', 'Grado', 2),
    ('Titulo 3', 'Grado', 3),
    ('Titulo 4', 'Grado', 1),
    ('Titulo 5', 'Grado', 2),
    ('Titulo 6', 'Grado', 3),
    ('Titulo 7', 'Grado', 1),
    ('Titulo 8', 'Grado', 2),
    ('Titulo 9', 'Grado', 3),
    ('Titulo 10', 'Grado', 1)
)
AS SOURCE ([Name], Grade, ResearchGroupId) ON TARGET.[Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Name], Grade, ResearchGroupId) VALUES ([Name], Grade, ResearchGroupId);


/*================= PROJECTS ================= */

MERGE INTO [dbo].[Project] AS TARGET
USING(VALUES
    (1, 0, 'Project 1', 1),
    (2, 1, 'Project 2', 2),
    (3, 0, 'Project 3', 1),
    (4, 1, 'Project 4', 3),
    (5, 0, 'Project 5', 1),
    (6, 1, 'Project 6', 2),
    (7, 0, 'Project 7', 1),
    (8, 1, 'Project 8', 1)
)
AS SOURCE ([Id], [Active], [Name], [ResearchGroupId]) ON TARGET.[Id] = SOURCE.[Id]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Id], [Active], [Name], [ResearchGroupId]) VALUES ([Id], [Active], [Name], [ResearchGroupId]);

/*================= PUBLICATIONS ================= */

MERGE INTO [dbo].[Publication] AS TARGET
USING(VALUES
    (1, 'Publication 1', '10.1109/CLEI52000.2020.00067', 1),
    (2, 'Publication 2', '10.1109/CLEI52000.2020.00062', 2),
    (3, 'Publication 3', '10.1109/CLEI52000.2020.00063', 1),
    (4, 'Publication 4', '10.1109/CLEI52000.2020.00066', 3),
    (5, 'Publication 5', '10.1109/CLEI52000.2020.00061', 1),
    (6, 'Publication 6', '10.1109/CLEI52000.2020.00060', 2),
    (7, 'Publication 7', '10.1109/CLEI52000.2020.00068', 1),
    (8, 'Publication 8', '10.1109/CLEI52000.2020.00065', 1)
)
AS SOURCE ([Id], [Title], [DOI], [ResearchGroupId]) ON TARGET.[Id] = SOURCE.[Id]
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Id], [Title], [DOI], [ResearchGroupId]) VALUES ([Id], [Title], [DOI], [ResearchGroupId]);