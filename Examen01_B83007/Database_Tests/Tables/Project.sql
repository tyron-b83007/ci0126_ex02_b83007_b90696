﻿CREATE TABLE [dbo].[Project]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Active] BIT NOT NULL DEFAULT 1, 
    [Name] NVARCHAR(150) NOT NULL, 
    [ResearchGroupId] INT NOT NULL,
    CONSTRAINT [FK_Project_Group] FOREIGN KEY ([ResearchGroupId]) REFERENCES [ResearchGroup]([id])
)
