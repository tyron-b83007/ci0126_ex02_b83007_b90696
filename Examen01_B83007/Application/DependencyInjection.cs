﻿using Application.Universities;
using Application.Universities.Implementations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplicationLayer(this IServiceCollection services)
        {            
            services.AddTransient<IUniversityService, UniversityService>();
            services.AddTransient<IResearchCenterService, ResearchCenterService>();
            return services;
        }
    }
}
