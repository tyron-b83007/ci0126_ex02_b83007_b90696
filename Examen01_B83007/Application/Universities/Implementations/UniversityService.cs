﻿using Domain.Core.Entities;
using Domain.Universities;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Universities.Implementations
{
    internal class UniversityService : IUniversityService
    {

        private readonly IUniversityRepository _repository;

        public UniversityService(IUniversityRepository repository)
        {
            _repository = repository;
        }

        public async Task AddUniversity(University university)
        {
            await _repository.SaveAsync(university);
        }

        public async Task<IEnumerable<University>> GetUniversitiesAsync()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<University?> GetUniversitiesByIdAsync(int id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task RemoveUniversity(University university)
        {            
            await _repository.DeleteUniversity(university);
        }

        public async Task<Country> GetCountryById(int id){
            return await _repository.GetCountryById(id);
        }
    }
}
