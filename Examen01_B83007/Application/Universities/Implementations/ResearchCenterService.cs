﻿using Domain.Universities;
using Domain.Universities.DTOs;
using Domain.Universities.Entities;
using Domain.Universities.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Universities.Implementations
{
    public class ResearchCenterService : IResearchCenterService
    {
        private readonly IResearchCenterRepository _centerRepository;

        public ResearchCenterService(IResearchCenterRepository repo) {
            _centerRepository = repo;
        }

        public async Task<ResearchCenter?> GetCenterByIdAsync(int id)
        {
            return await _centerRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<ResearchCenter>> GetCentersAsync()
        {
            return await _centerRepository.GetAllAsync();
        }

        public async Task<ResearchGroup?> GetGroupByIdAsync(int id)
        {
            return await _centerRepository.GetResearchGroupByIdAsync(id);
        }

        public async Task<IEnumerable<ResearchGroup>> GetGroupsAsync()
        {
            return await _centerRepository.GetAllResearchGroupsAsync();
        }

        public async Task<IEnumerable<ResearchGroup>> GetResearchGroupsByCenterAsync(int idCenter)
        {
            return await _centerRepository.GetAllResearchGroupsByCenterAsync(idCenter);
        }

        public async Task<CollaboratorsDTO> GetCollaborators(int idGroup)
        {
            return await _centerRepository.GetCollaborators(idGroup);
        }

        public async Task<IEnumerable<Project>> GetProjects(int idGroup)
        {
            return await _centerRepository.GetProjects(idGroup);
        }

        public async Task<IEnumerable<Publication>> GetPublications(int idGroup)
        {
            return await _centerRepository.GetPublications(idGroup);
        }

        public double GetBudget(ResearchGroup group)
        {
            double result = -1;

            if(group != null)
            {
                var calc = new Calculator();
                result = calc.GetBudget(group);
            }
            return result;
        }
    }
}
