﻿using Domain.Universities.DTOs;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Universities
{
    public interface IResearchCenterService
    {
        /// <summary>
        /// Get all the research centers in the database
        /// </summary>
        /// <returns>Research centers</returns>
        Task<IEnumerable<ResearchCenter>> GetCentersAsync();
        /// <summary>
        /// Get a research center by the ID
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>Research Center</returns>
        Task<ResearchCenter?> GetCenterByIdAsync(int id);
        /// <summary>
        /// Get all the research groups in the database
        /// </summary>
        /// <returns>Research Group</returns>
        Task<IEnumerable<ResearchGroup>> GetGroupsAsync();
        /// <summary>
        /// Get a research group given the ID
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>Research Group</returns>
        Task<ResearchGroup?> GetGroupByIdAsync(int id);
        /// <summary>
        /// Get all the research groups of a given center
        /// </summary>
        /// <param name="idCenter">ID research center</param>
        /// <returns>List of research centers</returns>
        Task<IEnumerable<ResearchGroup>> GetResearchGroupsByCenterAsync(int idCenter);
        /// <summary>
        /// Get the collaborators of a given research group
        /// </summary>
        /// <param name="idGroup">ID research group</param>
        /// <returns>List of collaborators</returns>
        Task<CollaboratorsDTO> GetCollaborators(int idGroup);
        /// <summary>
        /// Get all the projects of a given research group
        /// </summary>
        /// <param name="idGroup">ID research group</param>
        /// <returns>List of projects</returns>
        Task<IEnumerable<Project>> GetProjects(int idGroup);
        /// <summary>
        /// Get all the publications of a given research group
        /// </summary>
        /// <param name="idGroup">ID research group</param>
        /// <returns>List of publications</returns>
        Task<IEnumerable<Publication>> GetPublications(int idGroup);
        /// <summary>
        /// Calculates the budget allowed for a group
        /// </summary>
        /// <param name="group">Research group</param>
        /// <returns>Budget in US dollars</returns>
        double GetBudget(ResearchGroup group);
    }
}
