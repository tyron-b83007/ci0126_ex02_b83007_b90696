﻿using Domain.Core.Entities;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Universities
{
    public interface IUniversityService
    {
        /// <summary>
        /// Get a list of all universities
        /// </summary>
        /// <returns>List of Universities</returns>
        Task<IEnumerable<University>> GetUniversitiesAsync();
        /// <summary>
        /// Get university given the id
        /// </summary>
        /// <param name="id">ID university</param>
        /// <returns>University</returns>
        Task<University?> GetUniversitiesByIdAsync(int id);
        /// <summary>
        /// Insert or update a university
        /// </summary>
        /// <param name="university">University to insert or update</param>
        /// <returns></returns>
        Task AddUniversity(University university);
        /// <summary>
        /// Delete a university
        /// </summary>
        /// <param name="university">University to delete</param>
        /// <returns></returns>
        Task RemoveUniversity(University university);
        /// <summary>
        /// Get a country by id
        /// </summary>
        /// <param name="id">Id country</param>
        /// <returns>Country</returns>
        Task<Country> GetCountryById(int id);
    }
}
