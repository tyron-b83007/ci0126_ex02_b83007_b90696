﻿using Domain.Core.Entities;
using Domain.Universities.Entities;
using Infrastructure.Core;
using Infrastructure.Universities.EntityMapping;
using Infrastructure.Universities.EntityMappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities
{
    public class UniversityDbContext : ApplicationDbContext
    {
        public UniversityDbContext(DbContextOptions<UniversityDbContext> options, ILogger<UniversityDbContext> logger) : base(options, logger)
        {
        }

        public DbSet<University> Universities { get; set; } = null!;
        public DbSet<Country> Countries { get; set; } = null!;
        public DbSet<Deparment> Deparments { get; set; } = null!;
        public DbSet<ResearchCenter> ResearchCenters { get; set; } = null!;
        public DbSet<ResearchGroup> ResearchGroups { get; set; } = null!;
        public DbSet<Collaborators> Collaborators { get; set; } = null!;
        public DbSet<Project> Projects { get; set; } = null!;
        public DbSet<Publication> Publications { get; set; } = null!;
        public DbSet<CollaboratorPerson> CollaboratorPeople { get; set; } = null!;
        public DbSet<Thesis> Theses { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UniversityMap());
            modelBuilder.ApplyConfiguration(new CountryMap());
            modelBuilder.ApplyConfiguration(new DeparmentMap());
            modelBuilder.ApplyConfiguration(new ResearchCenterMap());
            modelBuilder.ApplyConfiguration(new ResearchGroupMap());
            modelBuilder.ApplyConfiguration(new CollaboratorsMap());
            modelBuilder.ApplyConfiguration(new PublicationMap());
            modelBuilder.ApplyConfiguration(new ProjectMap());
            modelBuilder.ApplyConfiguration(new CollaboratorPersonMap());
            modelBuilder.ApplyConfiguration(new ThesisMap());
        }

    }
}
