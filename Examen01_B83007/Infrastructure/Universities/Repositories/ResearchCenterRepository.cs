﻿using Domain.Core.Repositories;
using Domain.Universities;
using Domain.Universities.DTOs;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.Repositories
{
    internal class ResearchCenterRepository : IResearchCenterRepository
    {
        private readonly UniversityDbContext _dbContext;
        public IUnitOfWork UnitOfWork => _dbContext;

        public ResearchCenterRepository(UniversityDbContext unitOfWork)
        {
            _dbContext = unitOfWork;
        }

        public async Task<IEnumerable<ResearchCenter>> GetAllAsync()
        {
            return await _dbContext.ResearchCenters.ToListAsync();
        }

        public async Task<ResearchCenter?> GetByIdAsync(int id)
        {
            return await _dbContext.ResearchCenters.FirstOrDefaultAsync(rc => rc.Id == id);
        }

        public async Task<IEnumerable<ResearchGroup>> GetAllResearchGroupsAsync()
        {
            return await _dbContext.ResearchGroups.ToListAsync();
        }

        public async Task<ResearchGroup?> GetResearchGroupByIdAsync(int id)
        {
            var result = await _dbContext.ResearchGroups
                .Include(g => g.ResearchCenter)
                .Include(g => g.Persons)
                .Include(g => g.Projects)
                .Include(g => g.PublishedTesis)
                .Include(g => g.Publications)
                .FirstOrDefaultAsync(rg => rg.Id == id);            

            return result;
        }

        public async Task<IEnumerable<ResearchGroup>> GetAllResearchGroupsByCenterAsync(int idCenter)
        {
            return await _dbContext.ResearchGroups.Where(g => g.ResearchCenter.Id == idCenter)
                .Include(g => g.ResearchCenter)
                .Include(g => g.Persons)
                .Include(g => g.Projects)
                .Include(g => g.PublishedTesis)
                .Include(g => g.Publications)
                .ToListAsync();
        }

        public async Task<CollaboratorsDTO> GetCollaborators(int idGroup)
        {
            CollaboratorsDTO collaborators = new CollaboratorsDTO();
            var other = await _dbContext.Collaborators.Where(c => c.Id == idGroup).ToListAsync();            
            foreach (Collaborators c in other)
            {
                var group = await _dbContext.ResearchGroups
                                                .Include(r => r.Deparment)            
                                                    .ThenInclude(d => d.University)
                                                        .ThenInclude(u => u.Country)
                                                .FirstOrDefaultAsync(rg => rg.Id == c.ResearchGroupId1);
                if (group != null)
                    collaborators.Collaborators.Add(group);
            }
            return collaborators;
        }

        public async Task<IEnumerable<Project>> GetProjects(int idGroup)
        {
            var result = await _dbContext.Projects.Where(p => p.Group.Id == idGroup).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<Publication>> GetPublications(int idGroup)
        {
            var result = await _dbContext.Publications.Where(p => p.Group.Id == idGroup).ToListAsync();
            return result;
        }

    }
}
