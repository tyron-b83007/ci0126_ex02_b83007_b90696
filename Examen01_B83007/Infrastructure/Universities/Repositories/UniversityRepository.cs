﻿using Domain.Core.Entities;
using Domain.Core.Repositories;
using Domain.Universities;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.Repositories
{
    internal class UniversityRepository : IUniversityRepository
    {
        private readonly UniversityDbContext _dbContext;
        public IUnitOfWork UnitOfWork => _dbContext;


        public UniversityRepository(UniversityDbContext context)
        {
            _dbContext = context;
        }

        public async Task<IEnumerable<University>> GetAllAsync()
        {
            var result = await _dbContext.Universities.Include(u => u.Deparments).Include(u => u.Country).OrderByDescending(u => u.Id).ToListAsync();
            return result;
        }

        public async Task<University?> GetByIdAsync(int id)
        {
            var result = await _dbContext.Universities.AsNoTracking().FirstOrDefaultAsync(u => u.Id == id);
            return result;
        }

        public async Task SaveAsync(University university)
        {
            if(university.Id > 0)//is update
            {
                var universityTracked = await _dbContext.Universities.FirstOrDefaultAsync(u => u.Id == university.Id);
                _dbContext.Entry(universityTracked).CurrentValues.SetValues(university);
            }
            else //is insert
            {
                _dbContext.Update(university);
            }            
            await _dbContext.SaveEntitiesAsync();
        }

        public async Task DeleteUniversity(University university)
        {
            _dbContext.Universities.Remove(university);            
            try
            {
                await _dbContext.SaveEntitiesAsync();
            }
            catch(ValidationException exc)
            {
                Debug.WriteLine(exc, $"{nameof(University)} validation exception: {exc?.Message}");
            }
            catch (DbUpdateException exc)
            {
                Debug.WriteLine(exc, $"{nameof(University)} db update error: {exc?.InnerException?.Message}");
            }
        }

        public async Task<Country> GetCountryById(int id)
        {
            return await _dbContext.Countries.FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}
