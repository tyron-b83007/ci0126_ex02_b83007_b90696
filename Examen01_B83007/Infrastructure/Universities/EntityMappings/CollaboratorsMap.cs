﻿using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMapping
{
    public class CollaboratorsMap : IEntityTypeConfiguration<Collaborators>
    {
        public void Configure(EntityTypeBuilder<Collaborators> builder)
        {
            builder.ToTable("ResearchGroupResearchGroup");
            builder.HasKey(c => new { c.Id, c.ResearchGroupId1 });
            builder.HasOne(c => c.Group).WithMany(b => b.Collaborators).HasForeignKey(cb => cb.Id);
            builder.HasOne(c => c.Group).WithMany(b => b.Collaborators).HasForeignKey(cb => cb.ResearchGroupId1);
        }
    }
}
