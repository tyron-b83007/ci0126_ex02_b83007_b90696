﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMapping
{
    public class ResearchGroupMap : IEntityTypeConfiguration<ResearchGroup>
    {
        public void Configure(EntityTypeBuilder<ResearchGroup> builder)
        {
            builder.ToTable("ResearchGroup");
            builder.HasKey(rg => rg.Id);
            builder.Property(rg => rg.Name)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnType("varchar()")
                .HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.Property(rg => rg.Url)
                .HasMaxLength(200)
                .HasColumnType("varchar()");
            builder.Property(rg => rg.Phone)
                .HasMaxLength(30)
                .HasColumnType("varchar()");
            builder.HasOne(g => g.Deparment).WithMany(d => d.ResearchGroups);
        }
    }
}
