﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMappings
{
    public class UniversityMap : IEntityTypeConfiguration<University>
    {
        public void Configure(EntityTypeBuilder<University> builder)
        {
            builder.ToTable("University");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Name).HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.Property(u => u.Url);
            builder.HasMany(u => u.Deparments).WithOne(d => d.University!);
            builder.HasOne(u => u.Country).WithMany(d => d.Universities);
        }
    }
}
