﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMappings
{
    public class ProjectMap : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Project");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Active);
            builder.Property(u => u.Name).HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.HasOne(u => u.Group).WithMany(g => g.Projects).HasForeignKey("ResearchGroupId");
        }
    }
}
