﻿using Domain.Core.Entities;
using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMappings
{
    public class CountryMap : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.ToTable("Country");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Name).HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.HasMany(d => d.Universities).WithOne(rg => rg.Country!);
        }
    }
}
