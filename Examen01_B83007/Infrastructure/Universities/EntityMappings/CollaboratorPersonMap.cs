﻿using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMappings
{
    public class CollaboratorPersonMap : IEntityTypeConfiguration<CollaboratorPerson>
    {
        public void Configure(EntityTypeBuilder<CollaboratorPerson> builder)
        {
            builder.ToTable("CollaboratorPerson");
            builder.HasKey(c => c.Name);
            builder.Property(c => c.Gender).IsRequired();
            builder.Property(c => c.Role).IsRequired();
            builder.HasOne(u => u.Group).WithMany(g => g.Persons).HasForeignKey("ResearchGroupId");
        }

}
}
