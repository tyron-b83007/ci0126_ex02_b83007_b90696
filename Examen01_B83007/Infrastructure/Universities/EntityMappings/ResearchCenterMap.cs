﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMapping
{
    public class ResearchCenterMap : IEntityTypeConfiguration<ResearchCenter>
    {
        public void Configure(EntityTypeBuilder<ResearchCenter> builder)
        {
            builder.ToTable("ResearchCenter");
            builder.HasKey(rc => rc.Id);
            builder.Property(rc => rc.Name).HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.Property(rc => rc.Url);
            builder.HasMany(rc => rc.ResearchGroups).WithOne(rg => rg.ResearchCenter!);
        }
    }
}
