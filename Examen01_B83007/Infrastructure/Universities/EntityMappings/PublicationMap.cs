﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMappings
{
    public class PublicationMap : IEntityTypeConfiguration<Publication>
    {
        public void Configure(EntityTypeBuilder<Publication> builder)
        {
            builder.ToTable("Publication");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.DOI);
            builder.Property(u => u.Title).HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.HasOne(u => u.Group).WithMany(g => g.Publications).HasForeignKey("ResearchGroupId");
        }
    }
}
