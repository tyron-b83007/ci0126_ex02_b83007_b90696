﻿using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Universities.EntityMappings
{
    public class ThesisMap : IEntityTypeConfiguration<Thesis>
    {
        public void Configure(EntityTypeBuilder<Thesis> builder)
        {
            builder.ToTable("Thesis");
            builder.HasKey(c => c.Name);
            builder.Property(c => c.Grade).IsRequired();
            builder.HasOne(t => t.PublicationGroup).WithMany(r => r.PublishedTesis).HasForeignKey("ResearchGroupId");
        }
    }
}
