﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Universities.EntityMappings
{
    public class DeparmentMap : IEntityTypeConfiguration<Deparment>
    {
        public void Configure(EntityTypeBuilder<Deparment> builder)
        {
            builder.ToTable("Deparment");
            builder.HasKey(d => d.Id);
            builder.Property(d => d.Name).HasConversion(r => r.Value, s => RequiredString.TryCreate(s).Success());
            builder.HasMany(d => d.ResearchGroups).WithOne(rg => rg.Deparment!);
        }
    }
}
