﻿using Domain.Universities;
using Infrastructure.Universities;
using Infrastructure.Universities.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructureLayer(this IServiceCollection services, string connectionString)
        {           

            services.AddDbContext<UniversityDbContext>(options => options.UseSqlServer(connectionString).EnableSensitiveDataLogging());
            services.AddScoped<IUniversityRepository, UniversityRepository>();
            services.AddScoped<IResearchCenterRepository, ResearchCenterRepository>();

            return services;
        }
    }
}
