﻿using Domain.Core.Repositories;
using Domain.Universities.DTOs;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities
{
    public interface IResearchCenterRepository : IRepository<ResearchCenter>
    {
        /// <summary>
        /// Get all the research centers in the database
        /// </summary>
        /// <returns>Research centers</returns>
        Task<IEnumerable<ResearchCenter>> GetAllAsync();
        /// <summary>
        /// Get a research center by the ID
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>Research Center</returns>
        Task<ResearchCenter?> GetByIdAsync(int id);

        /// <summary>
        /// Get a CollaboratorBelongsToGroup by the ID of the group
        /// </summary>
        /// <param name="id">Group id</param>
        /// <returns>CollaboratorBelongsToGroup</returns>
        //Task<Thesis> GetRelationsByIdAsync(int id);
        /// <summary>
        /// Get all the research groups in the database
        /// </summary>
        /// <returns>Research Group</returns>
        Task<IEnumerable<ResearchGroup>> GetAllResearchGroupsAsync();
        /// <summary>
        /// Get a research group given the ID
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>Research Group</returns>
        Task<ResearchGroup?> GetResearchGroupByIdAsync(int id);
        /// <summary>
        /// Get all the research groups of a given center
        /// </summary>
        /// <param name="idCenter">ID research center</param>
        /// <returns>List of research centers</returns>
        Task<IEnumerable<ResearchGroup>> GetAllResearchGroupsByCenterAsync(int idCenter);
        /// <summary>
        /// Get the collaborators of a given research group
        /// </summary>
        /// <param name="idGroup">ID research group</param>
        /// <returns>List of collaborators</returns>
        Task<CollaboratorsDTO> GetCollaborators(int idGroup);
        /// <summary>
        /// Get all the projects of a given research group
        /// </summary>
        /// <param name="idGroup">ID research group</param>
        /// <returns>List of projects</returns>
        Task<IEnumerable<Project>> GetProjects(int idGroup);
        /// <summary>
        /// Get all the publications of a given research group
        /// </summary>
        /// <param name="idGroup">ID research group</param>
        /// <returns>List of publications</returns>
        Task<IEnumerable<Publication>> GetPublications(int idGroup);
    }
}
