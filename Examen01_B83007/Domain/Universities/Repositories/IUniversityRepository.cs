﻿using Domain.Core.Entities;
using Domain.Core.Repositories;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities
{
    public interface IUniversityRepository : IRepository<University>
    {
        /// <summary>
        /// Insert or update a university
        /// </summary>
        /// <param name="university">University to insert or update</param>
        /// <returns></returns>
        Task SaveAsync(University university);
        /// <summary>
        /// Get a list of all universities
        /// </summary>
        /// <returns>List of Universities</returns>
        Task<IEnumerable<University>> GetAllAsync();
        /// <summary>
        /// Get university given the id
        /// </summary>
        /// <param name="id">ID university</param>
        /// <returns>University</returns>
        Task<University?> GetByIdAsync(int id);
        /// <summary>
        /// Delete a university
        /// </summary>
        /// <param name="university">University to delete</param>
        /// <returns></returns>
        Task DeleteUniversity(University university);
        /// <summary>
        /// Get a country by id
        /// </summary>
        /// <param name="id">Id country</param>
        /// <returns>Country</returns>
        Task<Country> GetCountryById(int id);
    }
}
