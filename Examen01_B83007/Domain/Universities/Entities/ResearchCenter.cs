﻿using Domain.Core.Entities;
using Domain.Core.Exceptions;
using Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class ResearchCenter : AggregateRoot
    {
        public RequiredString Name { get; }
        public string? Url { get; }

        private readonly List<ResearchGroup> _researchGroups;
        public IReadOnlyCollection<ResearchGroup> ResearchGroups => _researchGroups.AsReadOnly();

        public ResearchCenter()
        {
            Name = null!;
            Url = null!;
            _researchGroups = null!;
        }

        public ResearchCenter(RequiredString name, string? url)
        {
            Name = name;
            Url = url;
            _researchGroups = new List<ResearchGroup>();
        }

        public void AddGroupToCenter(ResearchGroup group)
        {
            if (_researchGroups.Exists(g => g == group))
                throw new DomainException("group is already in the center.");
            if (_researchGroups.Exists(g => g.Id == group.Id))
                throw new DomainException("A group with the same id is already registered in the center.");

            _researchGroups.Add(group);
            group.AssignGroup(this);
        }

        public void RemvoeGroupFromCenter(ResearchGroup group)
        {
            if (_researchGroups.Exists(g => g == group))
            {
                _researchGroups.Remove(group);
                group.AssignGroup(null);
            }
        }
    }
}
