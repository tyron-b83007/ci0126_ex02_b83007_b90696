﻿using Domain.Core.Entities;
using Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class Project : AggregateRoot
    {
        public RequiredString Name { get; set; }
        public bool Active { get; set; }

        public ResearchGroup Group { get; }

        public Project()
        {
            Name = null!;
            Active = false;
            Group = null!;
        }

        public Project(RequiredString name, bool state, ResearchGroup group)
        {
            Name = name;
            Active = state;
            Group = group;
        }
    }
}
