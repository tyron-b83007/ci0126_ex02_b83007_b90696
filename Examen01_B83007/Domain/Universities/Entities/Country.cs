﻿using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Core.Entities
{
    public class Country : AggregateRoot
    {
        public RequiredString Name { get; }

        private readonly List<University> _universities;
        public virtual IReadOnlyCollection<University> Universities => _universities.AsReadOnly();

        public Country()
        {
            Name = null!;
            _universities = null!;
        }

        public Country(RequiredString name)
        {
            Name = name;
            _universities = new List<University>();
        }
    }
}
