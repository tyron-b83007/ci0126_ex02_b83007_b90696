﻿using Domain.Core.Entities;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class ResearchGroup : AggregateRoot
    {
        public RequiredString Name { get; }
        public string? Url  { get;  }
        public string? Phone  { get; }

        public ResearchCenter? ResearchCenter { get; set; }
        public Deparment? Deparment { get; set; }

        public List<Collaborators> _collaborators;
        public IReadOnlyCollection<Collaborators> Collaborators => _collaborators.AsReadOnly();

        public List<Publication> _publications;
        public IReadOnlyCollection<Publication> Publications => _publications.AsReadOnly();

        public List<Project> _projects;
        public IReadOnlyCollection<Project> Projects => _projects.AsReadOnly();

        public List<Thesis> PublishedTesis { get; set; }

        public List<CollaboratorPerson> Persons { get; set; }

        public ResearchGroup() {
            Name = null!;
            Url = null!;
            Phone = null!;
            ResearchCenter = null!;
            Deparment = null!;
            _collaborators = null!;
            _publications = null!;
            _projects = null!;
            Persons = null!;
        }

        public ResearchGroup(RequiredString name, string? url, string? phone, ResearchCenter researchCenter, Deparment? deparment)
        {
            Name = name;
            Url = url;
            Phone = phone;
            ResearchCenter = researchCenter;
            Deparment = deparment;
            _collaborators = new List<Collaborators>();
            _publications = new List<Publication>();
            _projects = new List<Project>();
            Persons = new List<CollaboratorPerson>();
        }

        public void AssignGroup(ResearchCenter? center)
        {
            ResearchCenter = center;
        }

        public void AssignDeparment(Deparment? deparment)
        {
            Deparment = deparment;
        }        
    }
}
