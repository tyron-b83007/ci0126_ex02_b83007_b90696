﻿using Domain.Core.Entities;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class Publication : AggregateRoot
    {
        public RequiredString Title { get; set; }
        public string? DOI { get; }

        public ResearchGroup Group { get; }

        public Publication()
        {
            Title = null!;
            DOI = null!;
            Group = null!;
        }

        public Publication(RequiredString title, string? doi, ResearchGroup group)
        {
            Title = title;
            DOI = doi;
            Group = group;
        }
    }
}
