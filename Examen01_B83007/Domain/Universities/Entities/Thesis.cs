﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class Thesis
    {
        public string Name { get; set; }
        public string Grade { get; set; }
        public int ResearchGroupId { get; set; }
        public ResearchGroup PublicationGroup { get; set; }
    }
}
