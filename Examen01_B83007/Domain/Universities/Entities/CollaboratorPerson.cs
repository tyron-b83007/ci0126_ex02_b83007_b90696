﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class CollaboratorPerson
    {
        public string Name { get; set;}
        public char Gender { get; set; }
        public string Role { get; set; }
        public ResearchGroup Group { get; set; }
    }
}
