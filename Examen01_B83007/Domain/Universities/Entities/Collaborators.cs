﻿using Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class Collaborators : AggregateRoot
    {
        public int ResearchGroupId1 { get; }

        public ResearchGroup Group { get; }
        public ResearchGroup Collaborator { get; }

        public Collaborators()
        {
            ResearchGroupId1 = 0;
            Group = null!;
            Collaborator = null!;
        }

        public Collaborators(ResearchGroup g, ResearchGroup c)
        {
            Group = g;
            Collaborator = c;
        }
     }
}
