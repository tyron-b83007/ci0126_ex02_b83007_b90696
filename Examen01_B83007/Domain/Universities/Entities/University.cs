﻿using Domain.Core.Entities;
using Domain.Core.Exceptions;
using Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class University : AggregateRoot
    {
        public RequiredString Name { get;  }

        public string? Url { get; }

        public Country Country { get;  }

        private readonly List<Deparment> _Deparments;
        public IReadOnlyCollection<Deparment> Deparments => _Deparments.AsReadOnly();

        public University()
        {
            Name = null!;
            Url = null!;
            Country = null!;
            _Deparments = null!;
        }

        public University(RequiredString name, string? url, Country country)
        {
            Name = name;
            Url = url;
            Country = country;
            _Deparments = new List<Deparment>();
        }

        public University(int id, RequiredString name, string? url, Country country)
        {
            Id = id;
            Name = name;
            Url = url;
            Country = country;
            _Deparments = new List<Deparment>();
        }

        public void AddDeparmentToUniversity(Deparment deparment)
        {
            if (_Deparments.Exists(d => d == deparment))
                throw new DomainException("deparment is already in the university.");
            if (_Deparments.Exists(d => d.Id == deparment.Id))
                throw new DomainException("A deparment with the same id is already registered in the university.");

            _Deparments.Add(deparment);
            deparment.AssignUniversity(this);
        }
    }
}
