﻿using Domain.Core.Entities;
using Domain.Core.Exceptions;
using Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.Entities
{
    public class Deparment : AggregateRoot
    {
        public RequiredString Name { get;  }

        public University University { get; set; }

        private readonly List<ResearchGroup> _researchGroups;
        public IReadOnlyCollection<ResearchGroup> ResearchGroups => _researchGroups.AsReadOnly();

        public Deparment()
        {
            Name = null!;
            University = null!;
            _researchGroups = null!;
        }

        public Deparment(RequiredString name, University uni)
        {
            Name = name;
            University = uni;
            _researchGroups = new List<ResearchGroup>();
        }

        public void AssignUniversity(University uni)
        {
            University = uni;
        }

        public void AddGroupToDeparment(ResearchGroup group)
        {
            if (_researchGroups.Exists(g => g == group))
                throw new DomainException("group is already in the deparment.");
            if (_researchGroups.Exists(g => g.Id == group.Id))
                throw new DomainException("A group with the same id is already registered in the deparment.");

            _researchGroups.Add(group);
            group.AssignDeparment(this);
        }

        public void RemvoeGroupFromDeparment(ResearchGroup group)
        {
            if (_researchGroups.Exists(g => g == group))
            {
                _researchGroups.Remove(group);
                group.AssignDeparment(null);
            }
        }
    }
}
