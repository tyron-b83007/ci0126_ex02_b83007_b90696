﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Universities.Entities;

namespace Domain.Universities.Utils
{
    public class Calculator
    {
        /// <summary>
        /// Calculates the budget allowed for a group
        /// </summary>
        /// <returns>Budget in US dollars</returns>
        public double GetBudget(ResearchGroup group)
        {
            double result = 800;
            var investigatorsCount = 0;
            var peopleCount = 0;
            var womenCount = 0;
            var studentsCount = 0;

            if (group.Persons != null)
            {
                investigatorsCount = group.Persons.Where(p => p.Role.Trim() == "Investigador").Count();
                peopleCount = group.Persons.Count;
                womenCount = group.Persons.Where(p => p.Gender == 'F').Count();
                studentsCount = group.Persons.Where(p => p.Role.Trim() == "Estudiante").Count();
            }

            if (group.Publications != null)
            {
                var pubs = group.Publications.Count();
                double partialResult = 200 * pubs;
                if (investigatorsCount > 10)
                {
                    partialResult += partialResult * 0.11;
                }
                else if (investigatorsCount > 6)
                {
                    partialResult += partialResult * 0.08;
                }
                result += partialResult;
            }

            if (group.Projects != null)
            {
                var activeProjects = group.Projects.Where(p => p.Active == true).Count();
                double partialResult = 150 * activeProjects;

                if (investigatorsCount > 8)
                {
                    partialResult += partialResult * 0.05;
                }
                else if (investigatorsCount > 5)
                {
                    partialResult += partialResult * 0.03;
                }

                if (studentsCount > 5)
                {
                    partialResult += partialResult * 0.05;
                }
                result += partialResult;
            }

            if (group.PublishedTesis != null)
            {
                var allThesesCount = group.PublishedTesis.Count;
                result += 100 * allThesesCount;
                var gradeThesesCount = group.PublishedTesis.Where(t => t.Grade.Trim() == "Grado").Count();
                double halfPercentage = (double)gradeThesesCount / (double)allThesesCount;
                if (halfPercentage >= 0.5)
                {
                    result += result * 0.02;
                }
            }

            //3% si al menos el 50% son mujeres
            double womenHalfPercentage = (double)womenCount / (double)peopleCount;
            if (womenHalfPercentage >= 0.5)
            {
                result += result * 0.03;
            }
            return result;
        }
    }
}
