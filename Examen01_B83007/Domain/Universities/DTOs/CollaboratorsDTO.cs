﻿using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Universities.DTOs
{
    public class CollaboratorsDTO
    {
        public List<ResearchGroup> Collaborators { get; set; } = new List<ResearchGroup>();
    }
}
