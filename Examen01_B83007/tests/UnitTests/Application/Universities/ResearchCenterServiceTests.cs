﻿using Application.Universities.Implementations;
using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities;
using Domain.Universities.Entities;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests.Application.Universities
{
    public class ResearchCenterServiceTests
    {
        private Mock<IResearchCenterRepository> mockRepository = new();

        private static readonly ResearchGroup Group = new ResearchGroup(RequiredString.TryCreate("Group Name").Success(), null, null, null, null);

        [Fact]
        public async Task GetGroupByIdAsyncInvalidReturnsNull()
        {
            //arrange
            ResearchGroup? groupNull = null!;
            mockRepository.Setup(i => i.GetResearchGroupByIdAsync(1)).ReturnsAsync(groupNull);
            var service = new ResearchCenterService(mockRepository.Object);

            //act
            var group = await service.GetGroupByIdAsync(-1);

            //assert
            group.Should().BeNull();
        }

        [Fact]
        public async Task GetGroupByIdAsyncReturnsValidGroup()
        {
            //arrange
            mockRepository.Setup(i => i.GetResearchGroupByIdAsync(1)).ReturnsAsync(Group);
            var service = new ResearchCenterService(mockRepository.Object);

            //act
            var group = await service.GetGroupByIdAsync(1);

            //assert
            group.Should().NotBeNull();
        }
    }
}
