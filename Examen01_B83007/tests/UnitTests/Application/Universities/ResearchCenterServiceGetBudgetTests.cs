﻿using Application.Universities.Implementations;
using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities;
using Domain.Universities.Entities;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests.Application.Universities
{
    public class ResearchCenterServiceGetBudgetTests
    {
        private Mock<IResearchCenterRepository> mockRepository = new();

        [Fact]
        public void GetBudgetNullReturnsNegative()
        {
            //arrange
            var service = new ResearchCenterService(mockRepository.Object);

            //act
            var result = service.GetBudget(null);

            //assert
            result.Should().Be(-1);
        }

        [Fact]
        public void GetBudgetReturnsValue()
        {
            //arrange
            var service = new ResearchCenterService(mockRepository.Object);
            ResearchGroup group = new ResearchGroup(RequiredString.TryCreate("Group Name").Success(), null, null, null, null);
            var utils = new Utils();
            group.Persons = utils.getXPeopleYInvestigatorZWomen(17, 11, 9);
            group._publications = utils.getXPublications(5);
            group._projects = utils.getXProyectsYActive(1, 1);
            group.PublishedTesis = utils.getXThesesYGrade(4, 3);

            //act
            var result = service.GetBudget(group);

            //assert
            result.Should().Be(2600.628975);
        }
    }
}
