﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Domain.Universities.Entities;
using Domain.Core.ValueObjects;
using Domain.Core.Helpers;
using Domain.Universities.Utils;


namespace UnitTests.Universities
{
    public class GetBudgetTests
    {
        [Fact]
        public void allConditionsMetTestV1()
        {
            //arrange
            var group = new ResearchGroup();
            var utils = new Utils();
            group.Persons = utils.getXPeopleYInvestigatorZWomen(17, 11, 9);
            group._publications = utils.getXPublications(5);
            group._projects = utils.getXProyectsYActive(1, 1);
            group.PublishedTesis = utils.getXThesesYGrade(4, 3);

            //act
            var calc = new Calculator();
            var result = calc.GetBudget(group);

            //assert
            result.Should().Be(2600.628975);
        }

        [Fact]
        public void allConditionsMetTestV2()
        {
            //arrange
            var group = new ResearchGroup();
            var utils = new Utils();
            group.Persons = utils.getXPeopleYInvestigatorZWomen(13, 7, 7);
            group._publications = utils.getXPublications(5);
            group._projects = utils.getXProyectsYActive(1, 1);
            group.PublishedTesis = utils.getXThesesYGrade(4, 3);

            //act
            var calc = new Calculator();
            var result = calc.GetBudget(group);

            //assert
            result.Should().Be(2565.801585);
        }

        [Fact]
        public void noConditionsMetTest()
        {
            //arrange
            var group = new ResearchGroup();
            var utils = new Utils();
            group.Persons = utils.getXPeopleYInvestigatorZWomen(4, 3, 1);
            group._publications = utils.getXPublications(0);
            group._projects = utils.getXProyectsYActive(0, 0);
            group.PublishedTesis = utils.getXThesesYGrade(0, 0);

            //act
            var calc = new Calculator();
            var result = calc.GetBudget(group);

            //assert
            result.Should().Be(800);
        }
    }
}
