﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Domain.Universities.Entities;
using Domain.Core.ValueObjects;
using Domain.Core.Helpers;
using Domain.Universities.Utils;

namespace UnitTests.Universities
{
    public class ResearchGroupTests
    {
        [Fact]
        public void nullConstructorTest()
        {
            //arrange
            var group = new ResearchGroup();
            //act
            //assert
            group.Name.Should().Be(null);
            group.Url.Should().Be(null);
            group.Phone.Should().Be(null);
            group.ResearchCenter.Should().Be(null);
            group.Deparment.Should().Be(null);
            group._collaborators.Should().BeNullOrEmpty();
            group._publications.Should().BeNullOrEmpty();
            group._projects.Should().BeNullOrEmpty();
            group.Persons.Should().BeNullOrEmpty();
        }

        [Fact]
        public void argumentsConstructorTest()
        {
            //arrange
            var centerName = RequiredString.TryCreate("CenterName").Success();
            var groupName = RequiredString.TryCreate("GroupName").Success();
            var deptName = RequiredString.TryCreate("DepartmentName").Success();
            var uni = new University();
            var center = new ResearchCenter(centerName,"URL");
            var dept = new Deparment(deptName, uni);
            var group = new ResearchGroup(groupName, "URL","8506-5234", center, dept);
            //act
            //assert
            group.Name.Should().Be(groupName);
            group.Url.Should().Be("URL");
            group.Phone.Should().Be("8506-5234");
            group.ResearchCenter.Name.Should().Be(centerName);
            group.Deparment.Name.Should().Be(deptName);
            group._collaborators.Should().NotBeNull();
            group._publications.Should().NotBeNull();
            group._projects.Should().NotBeNull();
            group.Persons.Should().NotBeNull();
        }

        [Fact]
        public void assignGroupTest()
        {
            //arrange
            var group = new ResearchGroup();
            var centerName = RequiredString.TryCreate("CenterName").Success();
            var center = new ResearchCenter(centerName, "URL");
            //act
            group.AssignGroup(center);
            //assert
            group.ResearchCenter.Name.Should().Be(centerName);
        }

        [Fact]
        public void assignDeparmentTest()
        {
            //arrange
            var group = new ResearchGroup();
            var deptName = RequiredString.TryCreate("DepartmentName").Success();
            var uni = new University();
            var dept = new Deparment(deptName, uni);
            //act
            group.AssignDeparment(dept);
            //assert
            group.Deparment.Name.Should().Be(deptName);
        }
    }
}
