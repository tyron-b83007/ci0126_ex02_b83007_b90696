﻿using Domain.Core.Helpers;
using Domain.Core.ValueObjects;
using Domain.Universities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class Utils
    {
        public List<Publication> getXPublications(int x)
        {
            var pubList = new List<Publication>();
            for (int i = 0; i < x; i++)
            {
                var publication = new Publication();
                var title = RequiredString.TryCreate("PubTitle").Success();
                publication.Title = title;
                pubList.Add(publication);
            }
            return pubList;
        }

        public List<Project> getXProyectsYActive(int x, int y)
        {
            var proyList = new List<Project>();
            for (int i = 0; i < x; i++)
            {
                var proyect = new Project();
                var name = RequiredString.TryCreate("ProyName").Success();
                proyect.Name = name;
                if (y-- > 0)
                {
                    proyect.Active = true;
                }
                else
                {
                    proyect.Active = false;
                }
                proyList.Add(proyect);
            }
            return proyList;
        }

        public List<Thesis> getXThesesYGrade(int x, int y)
        {
            var thesisList = new List<Thesis>();
            for (int i = 0; i < x; i++)
            {
                var thesis = new Thesis();
                var name = "ThesisName";
                thesis.Name = name;
                if (y-- > 0)
                {
                    thesis.Grade = "Grado";
                }
                else
                {
                    thesis.Grade = "Posgrado";
                }
                thesisList.Add(thesis);
            }
            return thesisList;
        }

        public List<CollaboratorPerson> getXPeopleYInvestigatorZWomen(int x, int y, int z)
        {
            var thesisList = new List<CollaboratorPerson>();
            for (int i = 0; i < x; i++)
            {
                var person = new CollaboratorPerson();
                var name = "PersonName";
                person.Name = name;
                if (y-- > 0)
                {
                    person.Role = "Investigador";
                }
                else
                {
                    person.Role = "Estudiante";
                }
                if (z-- > 0)
                {
                    person.Gender = 'F';
                }
                else
                {
                    person.Gender = 'M';
                }
                thesisList.Add(person);
            }
            return thesisList;
        }

    }
}
