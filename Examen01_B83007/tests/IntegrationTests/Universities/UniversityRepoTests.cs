﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Server;
using Microsoft.Extensions.DependencyInjection;
using Domain.Universities;
using FluentAssertions;

namespace IntegrationTests.Universities
{
    public class UniversityRepoTests : IClassFixture<UniversityWebAppFactory<Startup>>
    {
        private readonly UniversityWebAppFactory<Startup> _factory;

        public UniversityRepoTests(UniversityWebAppFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetAllReturnsAllUniversities()
        {
            //arrange
            var repository = _factory.Server.Services.GetRequiredService<IUniversityRepository>();

            //act
            var uni = await repository.GetAllAsync();

            //assert
            uni.Should().HaveCount(7);
        }
    }
}
