﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Server;
using Microsoft.Extensions.DependencyInjection;
using Domain.Universities;
using FluentAssertions;

namespace IntegrationTests.Universities
{
    public class ResearchCenterRepoTests : IClassFixture<UniversityWebAppFactory<Startup>>
    {
        private readonly UniversityWebAppFactory<Startup> _factory;

        public ResearchCenterRepoTests(UniversityWebAppFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetGroupInvalidReturnsNull()
        {
            //arrange
            var repository = _factory.Server.Services.GetRequiredService<IResearchCenterRepository>();

            //act
            var group = await repository.GetResearchGroupByIdAsync(-1);

            //assert
            group.Should().BeNull();
        }

        [Fact]
        public async Task GetGroupReturnsValidGroup()
        {
            //arrange
            var repository = _factory.Server.Services.GetRequiredService<IResearchCenterRepository>();

            //act
            var group = await repository.GetResearchGroupByIdAsync(1);

            //assert
            group.Should().NotBeNull();
        }
    }
}
